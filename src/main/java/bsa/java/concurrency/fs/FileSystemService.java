package bsa.java.concurrency.fs;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.concurrent.CompletableFuture;

@Service

public class FileSystemService implements FileSystem {

  @Value(value = "${fs.root}")
    private String ROOT;
 @Value(value = "${fs.local_url}")
    private String LOCAL_URL ;
    @PostConstruct
    public void init() {
        new File(ROOT).mkdir();
    }

    @Override
    public CompletableFuture<String> saveFile(String path, byte[] file) {
        return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        Path filepath = Path.of(ROOT + "/" + path);
                        Files.write(filepath, file);
                        return LOCAL_URL + filepath.getFileName();
                    } catch (Exception e) {
                        throw new RuntimeException("Could not save the file. Error: " + e.getMessage());
                    }
                });

    }
 @Override
    public CompletableFuture<Void> deleteFile(String url) {
        return CompletableFuture.supplyAsync(
                () -> {
                    String path = url.replace(LOCAL_URL, "");
                    try {
                        Files.delete(Path.of(ROOT + "/" + path));
                    } catch (IOException e) {
                        throw new RuntimeException("Could not delete the file. Error: " + e.getMessage());
                    }
                    return null;
                });
    }

    @Override
    public CompletableFuture<Void> deleteAllFiles() {
          return CompletableFuture.supplyAsync(
                () -> {
                    try {
                        Files.walk(Path.of(ROOT))
                                .sorted(Comparator.reverseOrder())
                                .forEach(path -> {
                                    try {
                                        if (path.compareTo(Path.of(ROOT)) != 0) {
                                            Files.delete(path);
                                        }
                                    } catch (IOException e) {
                                        throw new RuntimeException("Could not delete the file. Error: " + e.getMessage());
                                    }
                                });
                       return null;
                    } catch (IOException e) {
                        throw new RuntimeException("Could not delete the files. Error: " + e.getMessage());
                    }
                });

    }
}
