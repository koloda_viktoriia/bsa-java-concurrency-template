package bsa.java.concurrency.hash;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.ByteArrayInputStream;

public class DHash {
    public long calculateHash(byte[] image) {
        try {
            var img = ImageIO.read(new ByteArrayInputStream(image));
            return calculateDHash(img);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public long calculateDHash(BufferedImage image) {
        long hash = 0;
        BufferedImage scaled = resize(grayScale(image), 9, 9);
        for (int i = 1; i < 9; i++) {
            for (int j = 1; j < 9; j++) {
                if ((scaled.getRGB(i, j) & 0b11111111) > (scaled.getRGB(i - 1, j - 1) & 0b11111111)) {
                    hash |= 1;
                }
                hash <<= 1;
            }
        }
        return hash;
    }

    public BufferedImage resize(BufferedImage image, int width, int height) {
        BufferedImage resizedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(image, 0, 0, width, height, null);
        g.dispose();
        return resizedImage;
    }

    private BufferedImage grayScale(BufferedImage image) {
        ColorConvertOp colorConvert = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
        colorConvert.filter(image, image);
        return image;
    }


}