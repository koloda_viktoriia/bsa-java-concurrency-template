package bsa.java.concurrency.exception;

public class ImageNotFoundException extends  Exception {
             public ImageNotFoundException() {
            super(String.format("Can not find image."));
        }

}
