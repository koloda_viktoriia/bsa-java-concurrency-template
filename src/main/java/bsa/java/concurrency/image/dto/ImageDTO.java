package bsa.java.concurrency.image.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ImageDTO {
    UUID id;
    long hash;
    String imageUrl;

}
