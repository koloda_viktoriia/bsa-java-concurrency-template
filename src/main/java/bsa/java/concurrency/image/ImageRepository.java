package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ImageRepository extends JpaRepository<Image, UUID> {

    @Query(value = "SELECT CAST(id AS VARCHAR) AS imageId ," +
            "((1- LENGTH(REPLACE(((images.hash\\:\\:bit(64))#(:hash\\:\\:bit(64)))\\:\\:text,'0',''))\\:\\:decimal/64)*100) " +
            "AS matchPercent , " +
            "image_url  AS imageUrl " +
            "FROM images " +
            "WHERE (1- LENGTH(REPLACE(((images.hash\\:\\:bit(64))#(:hash\\:\\:bit(64)))\\:\\:text,'0',''))\\:\\:decimal/64)>= :threshold ",
            nativeQuery = true)
    List<SearchResultDTO> findAllByHash(long hash, double threshold);
}
