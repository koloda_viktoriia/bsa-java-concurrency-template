package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.ImageNotFoundException;
import bsa.java.concurrency.fs.FileSystemService;
import bsa.java.concurrency.hash.DHash;
import bsa.java.concurrency.image.dto.ImageDTO;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class ImageService {
    private ImageRepository imageRepository;
    @Autowired
    private FileSystemService fileSystemService;

    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;

    }

    public void uploadImages(MultipartFile[] files) {
        Arrays.stream(files).forEach(file -> {
            try {
                var urlTask = fileSystemService.saveFile(file.getOriginalFilename(), file.getBytes());
                var hash = new DHash().calculateHash(file.getBytes());
                urlTask.whenComplete((result, exception) -> {
                    ImageDTO image = new ImageDTO();
                    image.setImageUrl(result);
                    image.setHash(hash);
                    imageRepository.save(Image.fromDto(image));
                });
            } catch (Exception e) {
                throw new RuntimeException("Could not save file. Error: " + e.getMessage());
            }
        });
    }

    public List<SearchResultDTO> searchMatches(MultipartFile file, double threshold) throws ImageNotFoundException {
        try {
            var hash = new DHash().calculateHash(file.getBytes());
            var resultSearch = imageRepository.findAllByHash(hash, threshold);
            if (!resultSearch.isEmpty()) {
                return resultSearch;
            }
            var urlTask = fileSystemService
                    .saveFile(file.getOriginalFilename(), file.getBytes())
                    .thenApply((result) -> {
                                ImageDTO image = new ImageDTO();
                                image.setImageUrl(result);
                                image.setHash(hash);
                                return imageRepository.save(Image.fromDto(image));
                            }
                    );
            throw new ImageNotFoundException();
        } catch (IOException e) {
            throw new RuntimeException("Could not save the file. Error: " + e.getMessage());
        }
    }

    public void deleteAll() {
        fileSystemService.deleteAllFiles();
        imageRepository.deleteAll();
    }

    public void deleteById(UUID imageId) throws ImageNotFoundException {
        Image image = imageRepository.findById(imageId).orElseThrow(() -> new ImageNotFoundException());
        fileSystemService.deleteFile(image.getImageUrl());
        imageRepository.deleteById(imageId);
    }
}
