package bsa.java.concurrency.image;

import bsa.java.concurrency.image.dto.ImageDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "images")
public class Image {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private long hash;

    @Column
    private String imageUrl;

    public static Image fromDto(ImageDTO image) {
        return Image.builder()
                .hash(image.getHash())
                .imageUrl(image.getImageUrl())
                .build();
    }

}
