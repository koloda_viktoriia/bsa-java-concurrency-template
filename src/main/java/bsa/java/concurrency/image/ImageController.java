package bsa.java.concurrency.image;

import bsa.java.concurrency.exception.ImageNotFoundException;
import bsa.java.concurrency.image.dto.SearchResultDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

@RestController
@RequestMapping("/image")
public class ImageController {

    private ImageService imageService;
    private static Logger logger = Logger.getLogger("Controller Logger");

    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @ExceptionHandler({ImageNotFoundException.class})
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

    @PostMapping("/batch")
    @ResponseStatus(HttpStatus.CREATED)
    public void batchUploadImages(@RequestParam("images") MultipartFile[] files) {
        logger.info("Upload images.");
        imageService.uploadImages(files);
    }

    @PostMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<SearchResultDTO> searchMatches(@RequestParam("image") MultipartFile file, @RequestParam(value = "threshold", defaultValue = "0.9") double threshold) throws ImageNotFoundException {
        logger.info("Search image with matches.");
        return imageService.searchMatches(file, threshold);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteImage(@PathVariable("id") UUID imageId) throws ImageNotFoundException {
        logger.info("Delete image by id.");
        imageService.deleteById(imageId);

    }

    @DeleteMapping("/purge")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void purgeImages() {
        logger.info("Delete all images on disk and clean db.");
        imageService.deleteAll();
    }
}
